package com.example.zadatak15_atos;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.Date;


@Database(entities = {Zadaci.class, Zaposlenici.class, Korisnik.class/*, ZadaciZaposleniciJoin.class*/}, exportSchema = false, version = 2)
public abstract class KreiranjeBaze extends RoomDatabase {
    private static final String DB_NAME = "Tvrtka";
    private static KreiranjeBaze instance;

    public abstract ZaposleniciBaza zaposleniciBaza();
    public abstract ZadaciBaza zadaciBaza();
    public abstract KorisnikBaza korisnikBaza();
    //public abstract ZadaciZaposleniciJoin zadaciZaposleniciJoinBaza();

    public static synchronized KreiranjeBaze getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), KreiranjeBaze.class, DB_NAME).
                    fallbackToDestructiveMigration().
                    addCallback(roomCallback).
                    build();
        }
        return  instance;
    }

    private static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopuniDbAsycTask(instance).execute();
        }
    };

    private static class PopuniDbAsycTask extends AsyncTask<Void, Void, Void>{
        private ZadaciBaza zadaciBaza;
        private ZaposleniciBaza zaposleniciBaza;
        private KorisnikBaza korisnikBaza;
        //private ZadaciZaposleniciJoinBaza zadaciZaposleniciJoinBaza;

        private PopuniDbAsycTask(KreiranjeBaze db){
            zadaciBaza = db.zadaciBaza();
            zaposleniciBaza = db.zaposleniciBaza();
            korisnikBaza = db.korisnikBaza();
            //zadaciZaposleniciJoinBaza = (ZadaciZaposleniciJoinBaza) db.zadaciZaposleniciJoinBaza();
        }

        String pocetnoVrijeme = java.text.DateFormat.getDateTimeInstance().format(new Date());
        String zavrsenoVrijeme = java.text.DateFormat.getDateTimeInstance().format(new Date());

        @Override
        protected Void doInBackground(Void... voids) {
            zadaciBaza.insertZadaci(new Zadaci( "Prvi zadatak", "U ovome zadatku napravite to i to", "Lagani", "Aktivno", 1, 5, pocetnoVrijeme, zavrsenoVrijeme));
            zadaciBaza.insertZadaci(new Zadaci( "Drugi zadatak", "U ovome zadatku napravite to i to", "Srednji", "Neaktivno", 3, 5, pocetnoVrijeme, zavrsenoVrijeme));
            zadaciBaza.insertZadaci(new Zadaci( "Treći zadatak", "U ovome zadatku napravite to i to", "Teški", "Aktivno", 7, 5, pocetnoVrijeme, zavrsenoVrijeme));


            zaposleniciBaza.insertZaposlenik(new Zaposlenici( "Marin", "Vuko", "FERIT", "1234123123"));
            zaposleniciBaza.insertZaposlenik(new Zaposlenici( "Mateo", "Jović", "FERIT", "732812932"));
            zaposleniciBaza.insertZaposlenik(new Zaposlenici( "Pero", "Perić", "FERIT", "23124212"));

            korisnikBaza.insertKorisnik(new Korisnik("MVuko123", "vuko.marin52@gmail.com", "A12345678b", "Admin"));
            korisnikBaza.insertKorisnik(new Korisnik("Jure123", "jure.juric1@gmail.com", "a12345678B", "Superuser"));
            korisnikBaza.insertKorisnik(new Korisnik("Pero123", "pero.peric1@gmail.com", "A12345678B", "Korisnik"));

            //zadaciZaposleniciJoinBaza.insert(new ZadaciZaposleniciJoin(1, 1));

            return null;
        }
    }


}


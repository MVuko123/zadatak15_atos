package com.example.zadatak15_atos;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Korisnik")
public class Korisnik {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "Korisničko ime")
    private String korisnickoIme;
    @ColumnInfo(name = "Email")
    private String email;
    @ColumnInfo(name = "Lozinka")
    private String lozinka;
    @ColumnInfo(name = "Uloga")
    private String uloga;

    public Korisnik(String korisnickoIme, String email, String lozinka, String uloga){
        this.korisnickoIme = korisnickoIme;
        this.email = email;
        this.lozinka = lozinka;
        this.uloga = uloga;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getUloga() {
        return uloga;
    }

    public void setUloga(String uloga) {
        this.uloga = uloga;
    }
}

package com.example.zadatak15_atos;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class ZaposleniciViewModel extends AndroidViewModel {

    private ZaposleniciRepozitoriji zaposleniciRepozitoriji;
    private LiveData<List<Zaposlenici>> sviZaposlenici;

    public ZaposleniciViewModel(@NonNull Application application) {
        super(application);
        zaposleniciRepozitoriji = new ZaposleniciRepozitoriji(application);
        sviZaposlenici = (LiveData<List<Zaposlenici>>) zaposleniciRepozitoriji.getZaposleniciList();
    }

    public void insert(Zaposlenici zaposlenici){
        zaposleniciRepozitoriji.insert(zaposlenici);
    }

    public void update(Zaposlenici zaposlenici){
        zaposleniciRepozitoriji.update(zaposlenici);
    }

    public void delete(Zaposlenici zaposlenici){
        zaposleniciRepozitoriji.delete(zaposlenici);
    }

    public void deleteAll() { zaposleniciRepozitoriji.deleteAll(); }

    public LiveData<List<Zaposlenici>> getSviZaposlenici(){
        return sviZaposlenici;
    }
}

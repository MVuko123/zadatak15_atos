package com.example.zadatak15_atos;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class ZaposleniciRepozitoriji {

    private ZaposleniciBaza zaposleniciBaza;
    private LiveData<List<Zaposlenici>> getZaposleniciList;

    public  ZaposleniciRepozitoriji(Application application){
        KreiranjeBaze baza = KreiranjeBaze.getInstance(application);

        zaposleniciBaza = baza.zaposleniciBaza();

        getZaposleniciList = zaposleniciBaza.getZaposleniciList();

    }

    public LiveData<List<Zaposlenici>> getZaposleniciList(){
        return getZaposleniciList;
    }

    public void insert(Zaposlenici zaposlenici){
        new ZaposleniciRepozitoriji.InsertZaposleniciAsyncTask(zaposleniciBaza).execute(zaposlenici);
    }

    public void update(Zaposlenici zaposlenici){
        new ZaposleniciRepozitoriji.UpdateZaposleniciAsyncTask(zaposleniciBaza).execute(zaposlenici);
    }

    public void delete(Zaposlenici zaposlenici){
        new ZaposleniciRepozitoriji.DeleteZaposleniciAsyncTask(zaposleniciBaza).execute(zaposlenici);
    }

    public void  deleteAll(){
        new ZaposleniciRepozitoriji.DeleteAllZaposleniciAsyncTask(zaposleniciBaza).execute();
    }



    private static class InsertZaposleniciAsyncTask extends AsyncTask<Zaposlenici, Void, Void> {
        private ZaposleniciBaza zaposleniciBaza;

        private InsertZaposleniciAsyncTask(ZaposleniciBaza zaposleniciBaza){
            this.zaposleniciBaza = zaposleniciBaza;
        }

        @Override
        protected Void doInBackground(Zaposlenici... zaposlenicis) {
            zaposleniciBaza.insertZaposlenik(zaposlenicis[0]);
            return null;
        }
    }

    private static class UpdateZaposleniciAsyncTask extends AsyncTask<Zaposlenici, Void, Void>{
        private ZaposleniciBaza zaposleniciBaza;

        private UpdateZaposleniciAsyncTask(ZaposleniciBaza zaposleniciBaza){
            this.zaposleniciBaza = zaposleniciBaza;
        }

        @Override
        protected Void doInBackground(Zaposlenici... zaposlenicis) {
            zaposleniciBaza.updateZaposlenik(zaposlenicis[0]);
            return null;
        }
    }

    private static class DeleteZaposleniciAsyncTask extends AsyncTask<Zaposlenici, Void, Void>{
        private ZaposleniciBaza zaposleniciBaza;

        private DeleteZaposleniciAsyncTask(ZaposleniciBaza zaposleniciBaza){
            this.zaposleniciBaza = zaposleniciBaza;
        }

        @Override
        protected Void doInBackground(Zaposlenici... zaposlenicis) {
            zaposleniciBaza.deleteZaposlenik(zaposlenicis[0]);
            return null;
        }
    }

    private static  class DeleteAllZaposleniciAsyncTask extends AsyncTask<Void, Void, Void>{
        private ZaposleniciBaza zaposleniciBaza;

        private DeleteAllZaposleniciAsyncTask(ZaposleniciBaza zaposleniciBaza){
            this.zaposleniciBaza = zaposleniciBaza;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            zaposleniciBaza.deleteAllZaposlenik();
            return null;
        }
    }
}

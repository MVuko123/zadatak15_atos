package com.example.zadatak15_atos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ZadaciBaza {

    @Query("Select * from Zadaci")
    LiveData<List<Zadaci>> getZadaciList();
    @Insert
    void insertZadaci(Zadaci zadaci);
    @Update
    void updateZadaci(Zadaci zadaci);
    @Delete
    void deleteZadaci(Zadaci zadaci);
    @Query("DELETE FROM Zadaci")
    void deleteAllZadaci();
}


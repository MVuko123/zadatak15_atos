package com.example.zadatak15_atos;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.lang.annotation.Native;
import java.util.Date;

@Entity(tableName = "Zadaci")
public class Zadaci {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "Naziv")
    private String naziv;
    @ColumnInfo(name = "Opis")
    private String opis;
    @ColumnInfo(name = "Tip")
    private String tip;
    @ColumnInfo(name = "Status")
    private String trenutniStatus;
    @ColumnInfo(name = "Kompleksnost")
    private int kompleksnost;
    @ColumnInfo(name = "Utrošeno Vrijeme")
    private int potrosenoVrijeme;
    @ColumnInfo(name = "Početno Vrijeme")
    private String pocetnoVrijeme; //java.text.DateFormat.getDateTimeInstance().format(new Date());
    @ColumnInfo(name = "Završno Vrijeme")
    private String zavrsnoVrijeme;

    public Zadaci( String naziv, String opis,String tip, String trenutniStatus, int kompleksnost, int potrosenoVrijeme, String pocetnoVrijeme, String zavrsnoVrijeme){
        this.naziv = naziv;
        this.opis = opis;
        this.tip = tip;
        this.trenutniStatus = trenutniStatus;
        this.kompleksnost = kompleksnost;
        this.potrosenoVrijeme = potrosenoVrijeme;
        this.pocetnoVrijeme = pocetnoVrijeme;
        this.zavrsnoVrijeme = zavrsnoVrijeme;
    }

    public int getId(){
        return id;
    }

    public String getNaziv(){
        return naziv;
    }

    public String getOpis(){
        return opis;
    }

    public String getTip(){
        return tip;
    }

    public String getTrenutniStatus(){
        return trenutniStatus;
    }

    public int getKompleksnost(){
        return kompleksnost;
    }

    public int getPotrosenoVrijeme(){
        return potrosenoVrijeme;
    }

    public String getPocetnoVrijeme(){
        return pocetnoVrijeme;
    }

    public String getZavrsnoVrijeme(){
        return zavrsnoVrijeme;
    }

    public void setId(int id){
        this.id = id;
    }

    public void setNaziv(String naziv){
        this.naziv = naziv;
    }

    public void setOpis(String opis){
        this.opis = opis;
    }

    public void setTip(String tip){
        this.tip = tip;
    }

    public void setTrenutniStatus(String trenutniStatus){
        this.trenutniStatus = trenutniStatus;
    }

    public void setKompleksnost(int kompleksnost){
        this.kompleksnost = kompleksnost;
    }

    public void setPotrosenoVrijeme(int potrosenoVrijeme){
        this.potrosenoVrijeme = potrosenoVrijeme;
    }

    public void setPocetnoVrijeme(String pocetnoVrijeme){
        this.pocetnoVrijeme = pocetnoVrijeme;
    }

    public void setZavrsnoVrijeme(String zavrsnoVrijeme){
        this.zavrsnoVrijeme = zavrsnoVrijeme;
    }

    @Override
    public String toString() {
        return "Zadaci{" +
                "id=" + id +
                ", naziv='" + naziv + '\'' +
                ", opis='" + opis + '\'' +
                ", tip='" + tip + '\'' +
                ", trenutniStatus='" + trenutniStatus + '\'' +
                ", kompleksnost=" + kompleksnost +
                ", potrosenoVrijeme=" + potrosenoVrijeme +
                ", pocetnoVrijeme='" + pocetnoVrijeme + '\'' +
                ", zavrsnoVrijeme='" + zavrsnoVrijeme + '\'' +
                '}';
    }
}
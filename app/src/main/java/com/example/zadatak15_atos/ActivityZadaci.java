package com.example.zadatak15_atos;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import kotlin.jvm.internal.Intrinsics;

public class ActivityZadaci extends AppCompatActivity {
    public static final int ADD_ZADATAK_REQUEST = 1;
    public static final int EDIT_ZADATAK_REQUEST = 2;
    private static final String TAG = "ActivityZadaci";

    private ZadaciViewModel zadaciViewModel;

    TextView ulogaText;

    @Override
    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zadaci);

        FloatingActionButton buttonDodajZadatak = findViewById(R.id.button_add_zadaci);
        //ulogaText = findViewById(R.id.text_uloga);
        String uloga = getIntent().getStringExtra("Uloga");
        //ulogaText.setText(uloga);
        //uloga = "Admin";
        if("Admin".equals(uloga) || "Superuser".equals(uloga)){
            buttonDodajZadatak.setVisibility(View.VISIBLE);
            buttonDodajZadatak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityZadaci.this, AddEditZadatakActivity.class);
                    startActivityForResult(intent, ADD_ZADATAK_REQUEST);
                }
            });
        }else {
            buttonDodajZadatak.setVisibility(View.GONE);
        }


        RecyclerView recyclerViewZadaci = findViewById(R.id.recycler_view_zadaci);
        recyclerViewZadaci.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewZadaci.setHasFixedSize(true);

        final ZadaciAdapter adapterZadaci = new ZadaciAdapter();
        recyclerViewZadaci.setAdapter(adapterZadaci);

        zadaciViewModel = new ViewModelProvider(this).get(ZadaciViewModel.class);
        zadaciViewModel.getSviZadaci().observe(this, new Observer<List<Zadaci>>() {
            @Override
            public void onChanged(List<Zadaci> zadacis) {
                adapterZadaci.setZadaciList(zadacis);
            }
        });

        if("Admin".equals(uloga)) {

            new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                    ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    zadaciViewModel.delete(adapterZadaci.getZadaciAt(viewHolder.getAdapterPosition()));
                    Toast.makeText(ActivityZadaci.this, "Zadatak obrisan", Toast.LENGTH_SHORT).show();
                }
            }).attachToRecyclerView(recyclerViewZadaci);

            adapterZadaci.setOnItemClickListener(new ZadaciAdapter.OnItemClickListenerZadaci() {
                @Override
                public void onItemClickZadaci(Zadaci zadaci) {
                    Log.d(TAG, "onItemClickZadaci: zadatak: " + zadaci);
                    Intent intent = new Intent(ActivityZadaci.this, AddEditZadatakActivity.class);
                    intent.putExtra(AddEditZadatakActivity.EXTRA_ID_ZADATAK, zadaci.getId());
                    intent.putExtra(AddEditZadatakActivity.EXTRA_NAZIV, zadaci.getNaziv());
                    intent.putExtra(AddEditZadatakActivity.EXTRA_OPIS, zadaci.getOpis());
                    intent.putExtra(AddEditZadatakActivity.EXTRA_TIP, zadaci.getTip());
                    intent.putExtra(AddEditZadatakActivity.EXTRA_STATUS, zadaci.getTrenutniStatus());
                    intent.putExtra(AddEditZadatakActivity.EXTRA_KOMPLEKSNOST, zadaci.getKompleksnost());
                    startActivityForResult(intent, EDIT_ZADATAK_REQUEST);
                }
            });
        }

        ActionBar actionbar = this.getSupportActionBar();
        Intrinsics.checkNotNull(actionbar);
        actionbar.setTitle((CharSequence)"Zadaci");
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    public boolean onSupportNavigateUp() {
        this.onBackPressed();
        String uloga = this.getIntent().getStringExtra("Uloga");
        return Boolean.parseBoolean(uloga);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADD_ZADATAK_REQUEST && resultCode == RESULT_OK){
            String naziv = data.getStringExtra(AddEditZadatakActivity.EXTRA_NAZIV);
            String opis = data.getStringExtra(AddEditZadatakActivity.EXTRA_OPIS);
            String tip = data.getStringExtra(AddEditZadatakActivity.EXTRA_TIP);
            String status = data.getStringExtra(AddEditZadatakActivity.EXTRA_STATUS);
            int kompleksnost = data.getIntExtra(AddEditZadatakActivity.EXTRA_KOMPLEKSNOST, 1);

            Zadaci zadaci = new Zadaci( naziv, opis, tip, status, kompleksnost, 1, "", "");
            zadaciViewModel.insert(zadaci);

            Toast.makeText(this, "Zadatak spremljen", Toast.LENGTH_SHORT).show();
        }else  if(requestCode == EDIT_ZADATAK_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(AddEditZadatakActivity.EXTRA_ID_ZADATAK, -1);

            if(id == -1){
                Toast.makeText(this, "Zadatak ne može biti ažuriran", Toast.LENGTH_SHORT).show();
                return;
            }

            String naziv = data.getStringExtra(AddEditZadatakActivity.EXTRA_NAZIV);
            String opis = data.getStringExtra(AddEditZadatakActivity.EXTRA_OPIS);
            String tip = data.getStringExtra(AddEditZadatakActivity.EXTRA_TIP);
            String status = data.getStringExtra(AddEditZadatakActivity.EXTRA_STATUS);
            int kompleksnost = data.getIntExtra(AddEditZadatakActivity.EXTRA_KOMPLEKSNOST, 1);

            Zadaci zadaci = new Zadaci( naziv, opis, tip, status, kompleksnost, 1, "", "");
            zadaciViewModel.update(zadaci);

            Toast.makeText(this, "Zadatak je ažuriran", Toast.LENGTH_SHORT).show();
        } else{
            Toast.makeText(this, "Zadatak nije spremljen", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        String uloga = getIntent().getStringExtra("Uloga");
        if("Admin".equals(uloga)) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.delete_zadaci_menu, menu);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String uloga = getIntent().getStringExtra("Uloga");
        if ("Admin".equals(uloga)) {
            switch (item.getItemId()) {
                case R.id.delete_all_zadaci:
                    zadaciViewModel.deleteAll();
                    Toast.makeText(this, "Svi zadaci obrisani", Toast.LENGTH_SHORT).show();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }else{
            return false;
        }
    }
}

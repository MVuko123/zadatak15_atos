package com.example.zadatak15_atos;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface KorisnikBaza {
    @Query("Select * from Korisnik")
    LiveData<List<Korisnik>> getKorisnikList();
    @Insert
    void insertKorisnik(Korisnik korisnik);

    @Query("SELECT * from korisnik where email=(:email) and lozinka=(:lozinka)")
    Korisnik prijava(String email, String lozinka);
}

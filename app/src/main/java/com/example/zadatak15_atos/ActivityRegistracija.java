package com.example.zadatak15_atos;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class ActivityRegistracija extends AppCompatActivity {
    public static final int ADD_KORISNIK_REQUEST = 1;

    private KorisnikViewModel korisnikViewModel;

    private Button button_registracija2;

    EditText korisnickoIme, email, lozinka;
    Button registracija2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registracija);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        korisnickoIme=findViewById(R.id.text_username);
        email = findViewById(R.id.text_email_registracija);
        lozinka = findViewById(R.id.edit_lozinka_registracija);
        registracija2 = findViewById(R.id.button_registracija2);

        registracija2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Korisnik korisnik = new Korisnik("", "", "", "Korisnik");
                korisnik.setKorisnickoIme(korisnickoIme.getText().toString());
                korisnik.setEmail(email.getText().toString());
                korisnik.setLozinka(lozinka.getText().toString());
                if(provjeraUpisa(korisnik)){
                    KreiranjeBaze kreiranjeBaze = KreiranjeBaze.getInstance(getApplicationContext());
                    KorisnikBaza korisnikBaza = kreiranjeBaze.korisnikBaza();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            korisnikBaza.insertKorisnik(korisnik);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Korisnik registriran", Toast.LENGTH_SHORT).show();
                                    button_registracija2 = (Button) findViewById(R.id.button_registracija);
                                    button_registracija2.setOnClickListener(view -> openActivityPrijava());
                                }
                            });
                        }
                    }).start();
                }else{
                    Toast.makeText(getApplicationContext(), "Morate unjeti potrebne podatke", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private Boolean provjeraUpisa(Korisnik korisnik){
        if(korisnik.getKorisnickoIme().isEmpty() ||
           korisnik.getEmail().isEmpty() ||
           korisnik.getLozinka().isEmpty()){
            return false;
        }
        return true;

    }

    public void openActivityPrijava(){
        Intent intent = new Intent(this, ActivityPrijava.class);
        startActivity(intent);
    }
}
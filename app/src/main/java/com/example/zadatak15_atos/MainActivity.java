package com.example.zadatak15_atos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    private Button button_zadaci;
    private Button button_zaposlenici;
    private Button button_prijava;

    TextView pozdrav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pozdrav = findViewById(R.id.text_pozdrav);
        String korisnickoIme = getIntent().getStringExtra("Korisnicko Ime");
        pozdrav.setText( korisnickoIme);

        String uloga = getIntent().getStringExtra("Uloga");


        /*KreiranjeBaze kreiranjeBaze = KreiranjeBaze.getInstance(getApplicationContext());
        KorisnikBaza korisnikBaza = kreiranjeBaze.korisnikBaza();
        Korisnik korisnik = new Korisnik("", "", "", "");*/

        button_zadaci = (Button) findViewById(R.id.button_zadaci);
        //button_zadaci.setOnClickListener(view -> openZadaciActivity());
        

        button_zadaci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActivityZadaci.class);
                intent.putExtra("Uloga", uloga);
                startActivity(intent);
            }
        });

        button_zaposlenici = (Button) findViewById(R.id.button_zaposlenici);
        button_zaposlenici.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActivityZaposlenici.class);
                intent.putExtra("Uloga", uloga);
                startActivity(intent);
            }
        });

        button_prijava = (Button) findViewById(R.id.button_prijava);
        button_prijava.setOnClickListener(view -> openActivityPrijava());

    }

    public void openZadaciActivity(){
        Intent intent = new Intent(MainActivity.this, ActivityZadaci.class);
        startActivity(intent);
    }

    public void openZaposleniciActivity(){
        Intent intent = new Intent(this, ActivityZaposlenici.class);
        startActivity(intent);
    }

    public void openActivityPrijava(){
        Intent intent = new Intent(this, ActivityPrijava.class);
        startActivity(intent);
    }
}
package com.example.zadatak15_atos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import kotlin.jvm.internal.Intrinsics;

public class AddEditZadatakActivity extends AppCompatActivity {
    public static final String EXTRA_ID_ZADATAK =
            "com.example.zadatak15_atos.EXTRA_ID_ZADATAK";
    public static final String EXTRA_NAZIV =
            "com.example.zadatak15_atos.EXTRA_NAZIV";
    public static final String EXTRA_OPIS =
            "com.example.zadatak15_atos.EXTRA_OPIS";
    public static final String EXTRA_TIP =
            "com.example.zadatak15_atos.EXTRA_TIP";
    public static final String EXTRA_STATUS =
            "com.example.zadatak15_atos.EXTRA_STATUS";
    public static final String EXTRA_KOMPLEKSNOST =
            "com.example.zadatak15_atos.EXTRA_KOMPLEKSNOST";

    private EditText editNaziv;
    private EditText editOpis;
    private EditText editTip;
    private EditText editStatus;
    private NumberPicker odabirKompleksnosti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_zadatak);

        editNaziv = findViewById(R.id.edit_naziv);
        editOpis = findViewById(R.id.edit_opis);
        editTip = findViewById(R.id.edit_tip);
        editStatus = findViewById(R.id.edit_status);
        odabirKompleksnosti = findViewById(R.id.odabir_kompleksnosti);

        odabirKompleksnosti.setMinValue(1);
        odabirKompleksnosti.setMaxValue(10);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();

        if(intent.hasExtra(EXTRA_ID_ZADATAK)){
            setTitle("Uredite zadatak");
            editNaziv.setText(intent.getStringExtra(EXTRA_NAZIV));
            editOpis.setText(intent.getStringExtra(EXTRA_OPIS));
            editTip.setText(intent.getStringExtra(EXTRA_TIP));
            editStatus.setText(intent.getStringExtra(EXTRA_STATUS));
            odabirKompleksnosti.setValue(intent.getIntExtra(EXTRA_KOMPLEKSNOST, 1));
        }else {
            setTitle("Dodajte zadatak");
        }

        ActionBar actionbar = this.getSupportActionBar();
        Intrinsics.checkNotNull(actionbar);
        actionbar.setTitle((CharSequence)"Zadaci");
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    public boolean onSupportNavigateUp() {
        this.onBackPressed();
        String uloga = this.getIntent().getStringExtra("Uloga");
        return Boolean.parseBoolean(uloga);
    }
    private void saveZadatak(){
        String naziv = editNaziv.getText().toString();
        String opis = editOpis.getText().toString();
        String tip = editTip.getText().toString();
        String status = editStatus.getText().toString();
        int kompleksnost = odabirKompleksnosti.getValue();

        if(naziv.trim().isEmpty() || opis.trim().isEmpty() || tip.trim().isEmpty() || status.trim().isEmpty()) {
            Toast.makeText(this, "Unesite tražene podatke", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_NAZIV, naziv);
        data.putExtra(EXTRA_OPIS, opis);
        data.putExtra(EXTRA_TIP, tip);
        data.putExtra(EXTRA_STATUS, status);
        data.putExtra(EXTRA_KOMPLEKSNOST, kompleksnost);

        int id = getIntent().getIntExtra(EXTRA_ID_ZADATAK, -1);
        if(id != -1){
            data.putExtra(EXTRA_ID_ZADATAK, id);
        }

        setResult(RESULT_OK, data);
        finish();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_zadatak_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_zadatak:
                saveZadatak();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

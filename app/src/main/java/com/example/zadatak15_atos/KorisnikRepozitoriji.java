package com.example.zadatak15_atos;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class KorisnikRepozitoriji {
    private KorisnikBaza korisnikBaza;

    private LiveData<List<Korisnik>> getKorisnikList;

    public KorisnikRepozitoriji(Application application){
        KreiranjeBaze baza = KreiranjeBaze.getInstance(application);

        korisnikBaza = baza.korisnikBaza();


        getKorisnikList = korisnikBaza.getKorisnikList();
    }


    public LiveData<List<Korisnik>> getKorisnikList(){
        return getKorisnikList;
    }

    public void insert(Korisnik korisnik){

    }

    private static class InsertKorisnikAsyncTask extends AsyncTask<Korisnik, Void, Void>{
        private KorisnikBaza korisnikBaza;

        private InsertKorisnikAsyncTask(KorisnikBaza korisnikBaza){
            this.korisnikBaza = korisnikBaza;
        }

        @Override
        protected Void doInBackground(Korisnik... korisniks) {
            korisnikBaza.insertKorisnik(korisniks[0]);
            return null;
        }
    }
}

package com.example.zadatak15_atos;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class KorisnikViewModel extends AndroidViewModel {

    private KorisnikRepozitoriji korisnikRepozitoriji;
    private LiveData<List<Korisnik>> sviKorisnici;

    public KorisnikViewModel(Application application){
        super(application);
        korisnikRepozitoriji = new KorisnikRepozitoriji(application);
        sviKorisnici = (LiveData<List<Korisnik>>) korisnikRepozitoriji.getKorisnikList();
    }

    public void insert(Korisnik korisnik) {
        korisnikRepozitoriji.insert(korisnik);
    }
}

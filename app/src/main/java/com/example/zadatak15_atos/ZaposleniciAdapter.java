package com.example.zadatak15_atos;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ZaposleniciAdapter extends RecyclerView.Adapter<ZaposleniciAdapter.ZaposleniciHolder> {

    private List<Zaposlenici> zaposlenicis = new ArrayList<>();

    private OnItemClickListenerZaposlenici listener;

    @NonNull
    @Override
    public ZaposleniciHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.zaposlenici_item, parent, false);
        return new ZaposleniciHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ZaposleniciHolder holder, int position) {
        //for(int i = 0; i < zaposlenicis.size(); i++) {
        Zaposlenici trenutniZaposlenik = zaposlenicis.get(position);
        holder.textViewTitle.setText(new StringBuilder().append(trenutniZaposlenik.getIme()).append(trenutniZaposlenik.getPrezime()).toString());
        holder.textViewDescription.setText(trenutniZaposlenik.getRadnoMjesto());
        holder.textViewPriority.setText(String.valueOf(trenutniZaposlenik.getId()));
        //}
    }

    @Override
    public int getItemCount() {
        return zaposlenicis.size();
    }

    public void setZaposleniciList(List<Zaposlenici> zaposlenicis) {
        this.zaposlenicis = zaposlenicis;
        notifyDataSetChanged();
    }

    public Zaposlenici getZaposlnikAt(int position) {
        return zaposlenicis.get(position);
    }

    class ZaposleniciHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewDescription;
        private TextView textViewPriority;

        public ZaposleniciHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            textViewPriority = itemView.findViewById(R.id.text_view_priority);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) //NO_POSITION je uvijek -1
                        listener.onItemClickZaposlenici(zaposlenicis.get(position));
                }
            });
        }
    }

    public interface OnItemClickListenerZaposlenici {
        void onItemClickZaposlenici(Zaposlenici zaposlenici);
    }

    public void setOnItemClickListener(OnItemClickListenerZaposlenici listener) {
        this.listener = listener;
    }
}

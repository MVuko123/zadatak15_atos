package com.example.zadatak15_atos;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import kotlin.jvm.internal.Intrinsics;

public class ActivityPrijava extends AppCompatActivity {

    private Button button_registracija;
    private Button button_prijava;

    EditText email, lozinka;
    Button prijava;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prijava);

        email = findViewById(R.id.email_prijava);
        lozinka = findViewById(R.id.lozinka_prijava);
        prijava = findViewById(R.id.button_prijava2);

        prijava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailText = email.getText().toString();
                String lozinkaText = lozinka.getText().toString();
                if(emailText.isEmpty() || lozinkaText.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Unesite potrebne podatke", Toast.LENGTH_SHORT).show();
                } else {
                    KreiranjeBaze kreiranjeBaze = KreiranjeBaze.getInstance(getApplicationContext());
                    KorisnikBaza korisnikBaza = kreiranjeBaze.korisnikBaza();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Korisnik korisnik = korisnikBaza.prijava(emailText, lozinkaText);
                            if(korisnik == null){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Krivo uneseni podaci", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }else{
                                String korisnickoIme = korisnik.getKorisnickoIme();
                                String uloga = korisnik.getUloga();
                                startActivity(new Intent(
                                        ActivityPrijava.this, MainActivity.class)
                                                     .putExtra("Korisnicko Ime", korisnickoIme)
                                                      .putExtra("Uloga", uloga));
                            }
                        }
                    }).start();
                }
            }
        });

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        button_registracija = (Button) findViewById(R.id.button_registracija);
        button_registracija.setOnClickListener(view -> openActivityRegistracija());

        /*button_prijava = (Button) findViewById((R.id.button_prijava2));
        button_prijava.setOnClickListener(view -> openMainActivity());*/


        ActionBar actionbar = this.getSupportActionBar();
        Intrinsics.checkNotNull(actionbar);
        actionbar.setTitle((CharSequence)"Prijava");
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    public boolean onSupportNavigateUp() {
        this.onBackPressed();
        String uloga = this.getIntent().getStringExtra("Uloga");
        return Boolean.parseBoolean(uloga);
    }
    public void openActivityRegistracija(){
        Intent intent = new Intent(this, ActivityRegistracija.class);
        startActivity(intent);
    }

    public void openMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
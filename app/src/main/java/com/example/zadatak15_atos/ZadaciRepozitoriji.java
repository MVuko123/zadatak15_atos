package com.example.zadatak15_atos;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class ZadaciRepozitoriji {
    private ZadaciBaza zadaciBaza;

    private LiveData<List<Zadaci>> getZadaciList;

    public ZadaciRepozitoriji(Application application){
        KreiranjeBaze baza = KreiranjeBaze.getInstance(application);

        zadaciBaza = baza.zadaciBaza();

        getZadaciList = zadaciBaza.getZadaciList();
    }

    public LiveData<List<Zadaci>> getZadaciList(){
        return getZadaciList;
    }

    public void insert(Zadaci zadaci){
        new ZadaciRepozitoriji.InsertZadaciAsyncTask(zadaciBaza).execute(zadaci);
    }

    public void update(Zadaci zadaci){
        new ZadaciRepozitoriji.UpdateZadaciAsyncTask(zadaciBaza).execute(zadaci);
    }

    public void delete(Zadaci zadaci){
        new ZadaciRepozitoriji.DeleteZadaciAsyncTask(zadaciBaza).execute(zadaci);
    }

    public void  deleteAll(){
        new ZadaciRepozitoriji.DeleteAllZadaciAsyncTask(zadaciBaza).execute();
    }

    private static class InsertZadaciAsyncTask extends AsyncTask<Zadaci, Void, Void> {
        private ZadaciBaza zadaciBaza;

        private InsertZadaciAsyncTask(ZadaciBaza zadaciBaza){
            this.zadaciBaza = zadaciBaza;
        }

        @Override
        protected Void doInBackground(Zadaci... zadacis) {
            zadaciBaza.insertZadaci(zadacis[0]);
            return null;
        }
    }

    private static class UpdateZadaciAsyncTask extends AsyncTask<Zadaci, Void, Void>{
        private ZadaciBaza zadaciBaza;

        private UpdateZadaciAsyncTask(ZadaciBaza zadaciBaza){
            this.zadaciBaza = zadaciBaza;
        }

        @Override
        protected Void doInBackground(Zadaci... zadacis) {
            zadaciBaza.updateZadaci(zadacis[0]);
            return null;
        }
    }

    private static class DeleteZadaciAsyncTask extends AsyncTask<Zadaci, Void, Void>{
        private ZadaciBaza zadaciBaza;

        private DeleteZadaciAsyncTask(ZadaciBaza zadaciBaza){
            this.zadaciBaza = zadaciBaza;
        }

        @Override
        protected Void doInBackground(Zadaci... zadacis) {
            zadaciBaza.deleteZadaci(zadacis[0]);
            return null;
        }
    }

    private static  class DeleteAllZadaciAsyncTask extends AsyncTask<Void, Void, Void>{
        private ZadaciBaza zadaciBaza;

        private DeleteAllZadaciAsyncTask(ZadaciBaza zadaciBaza){
            this.zadaciBaza = zadaciBaza;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            zadaciBaza.deleteAllZadaci();
            return null;
        }
    }

}

package com.example.zadatak15_atos;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import kotlin.jvm.internal.Intrinsics;

public class ActivityZaposlenici extends AppCompatActivity {
    public static final int ADD_ZAPOSLENICI_REQUEST = 1;
    public static final int EDIT_ZAPOSLENICI_REQUEST = 2;

    private ZaposleniciViewModel zaposleniciViewModel;

    @Override
    protected  void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zaposlenici);

        FloatingActionButton buttonDodajZaposlenika = findViewById(R.id.button_add_zaposlenici);
        //ulogaText = findViewById(R.id.text_uloga);
        String uloga = getIntent().getStringExtra("Uloga");
        //ulogaText.setText(uloga);
        //uloga = "Admin";
        if("Admin".equals(uloga) || "Superuser".equals(uloga)){
            buttonDodajZaposlenika.setVisibility(View.VISIBLE);
            buttonDodajZaposlenika.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityZaposlenici.this, AddEditZaposlenikActivity.class);
                    startActivityForResult(intent, ADD_ZAPOSLENICI_REQUEST);
                }
            });
        }else {
            buttonDodajZaposlenika.setVisibility(View.GONE);
        }


        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        final ZaposleniciAdapter adapterZaposlenici = new ZaposleniciAdapter();
        recyclerView.setAdapter(adapterZaposlenici);


        zaposleniciViewModel = new ViewModelProvider(this).get(ZaposleniciViewModel.class);
        zaposleniciViewModel.getSviZaposlenici().observe(this, new Observer<List<Zaposlenici>>() {
            @Override
            public void onChanged(List<Zaposlenici> zaposlenicis) {
                //Toast.makeText(MainActivity.this, "Zaposlenici", Toast.LENGTH_SHORT).show();
                adapterZaposlenici.setZaposleniciList(zaposlenicis);
            }
        });

        if("Admin".equals(uloga)) {

            new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                    ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                    return false; //drag and drop
                }

                @Override
                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                    zaposleniciViewModel.delete(adapterZaposlenici.getZaposlnikAt(viewHolder.getAdapterPosition()));
                    Toast.makeText(ActivityZaposlenici.this, "Zaposlenik obrisan", Toast.LENGTH_SHORT).show();
                }
            }).attachToRecyclerView(recyclerView);

            adapterZaposlenici.setOnItemClickListener(new ZaposleniciAdapter.OnItemClickListenerZaposlenici() {
                @Override
                public void onItemClickZaposlenici(Zaposlenici zaposlenici) {
                    Intent intent = new Intent(ActivityZaposlenici.this, AddEditZaposlenikActivity.class);
                    intent.putExtra(AddEditZaposlenikActivity.EXTRA_ID_ZAPOSLENIK, zaposlenici.getId());
                    intent.putExtra(AddEditZaposlenikActivity.EXTRA_IME, zaposlenici.getIme());
                    intent.putExtra(AddEditZaposlenikActivity.EXTRA_PREZIME, zaposlenici.getPrezime());
                    intent.putExtra(AddEditZaposlenikActivity.EXTRA_RADNOMJESTO, zaposlenici.getRadnoMjesto());
                    intent.putExtra(AddEditZaposlenikActivity.EXTRA_OIB, zaposlenici.getOib());
                    startActivityForResult(intent, EDIT_ZAPOSLENICI_REQUEST);
                }
            });
        }

        ActionBar actionbar = this.getSupportActionBar();
        Intrinsics.checkNotNull(actionbar);
        actionbar.setTitle((CharSequence)"Zaposlenici");
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    public boolean onSupportNavigateUp() {
        this.onBackPressed();
        String uloga = this.getIntent().getStringExtra("Uloga");
        return Boolean.parseBoolean(uloga);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == ADD_ZAPOSLENICI_REQUEST && resultCode == RESULT_OK){
            String ime = data.getStringExtra(AddEditZaposlenikActivity.EXTRA_IME);
            String prezime = data.getStringExtra(AddEditZaposlenikActivity.EXTRA_PREZIME);
            String radnoMjesto = data.getStringExtra(AddEditZaposlenikActivity.EXTRA_RADNOMJESTO);
            String oib = data.getStringExtra(AddEditZaposlenikActivity.EXTRA_OIB);

            Zaposlenici zaposlenici = new Zaposlenici(ime, prezime, radnoMjesto, oib);
            zaposleniciViewModel.insert(zaposlenici);

            Toast.makeText(this, "Zaposlenik spremljen", Toast.LENGTH_SHORT).show();
        }else  if(requestCode == EDIT_ZAPOSLENICI_REQUEST && resultCode == RESULT_OK) {
            int id = data.getIntExtra(AddEditZaposlenikActivity.EXTRA_ID_ZAPOSLENIK, -1);

            if(id == -1){
                Toast.makeText(this, "Zaposlenik ne može biti ažuriran", Toast.LENGTH_SHORT).show();
                return;
            }

            String ime = data.getStringExtra(AddEditZaposlenikActivity.EXTRA_IME);
            String prezime = data.getStringExtra(AddEditZaposlenikActivity.EXTRA_PREZIME);
            String radnoMjesto = data.getStringExtra(AddEditZaposlenikActivity.EXTRA_RADNOMJESTO);
            String oib = data.getStringExtra(AddEditZaposlenikActivity.EXTRA_OIB);

            Zaposlenici zaposlenici = new Zaposlenici(ime, prezime, radnoMjesto, oib);
            zaposlenici.setId(id);
            zaposleniciViewModel.update(zaposlenici);

            Toast.makeText(this, "Zaposlenik je ažuriran", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Zaposlenik nije spremljen", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        String uloga = getIntent().getStringExtra("Uloga");
        if("Admin".equals(uloga)) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.delete_zaposlenici_menu, menu);
            return true;
        }else{
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String uloga = getIntent().getStringExtra("Uloga");
        if("Admin".equals(uloga)) {
            switch (item.getItemId()) {
                case R.id.delete_all_zaposlenici:
                    zaposleniciViewModel.deleteAll();
                    Toast.makeText(this, "Svi zaposlenici obrisani", Toast.LENGTH_SHORT).show();
                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }
        }else{
            return false;
        }
    }
}

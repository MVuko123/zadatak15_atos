package com.example.zadatak15_atos;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ZadaciAdapter extends RecyclerView.Adapter<ZadaciAdapter.ZadaciHolder> {

    private List<Zadaci> zadacis = new ArrayList<>();

    private OnItemClickListenerZadaci listener;

    @NonNull
    @Override
    public ZadaciHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.zadaci_item, parent, false);
        return new ZadaciHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ZadaciHolder holder, int position) {
        //for(int i = 0; i < zaposlenicis.size(); i++) {
        Zadaci trenutniZadaci = zadacis.get(position);
        holder.textViewTitle.setText(trenutniZadaci.getNaziv());
        holder.textViewDescription.setText(trenutniZadaci.getOpis());
        holder.textViewPriority.setText(String.valueOf(trenutniZadaci.getId()));
        //}
    }

    @Override
    public int getItemCount() {
        return zadacis.size();
    }

    public void setZadaciList(List<Zadaci>  zadacis){
        this.zadacis = zadacis;
        notifyDataSetChanged();
    }


    public Zadaci getZadaciAt(int position){
        return  zadacis.get(position);
    }

    class ZadaciHolder extends RecyclerView.ViewHolder{
        private TextView textViewTitle;
        private TextView textViewDescription;
        private TextView textViewPriority;

        public ZadaciHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            textViewPriority = itemView.findViewById(R.id.text_view_priority);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) //NO_POSITION je uvijek -1
                        listener.onItemClickZadaci(zadacis.get(position));
                }
            });
        }
    }

    public interface OnItemClickListenerZadaci {
        void onItemClickZadaci(Zadaci zadaci);
    }

    public void setOnItemClickListener(OnItemClickListenerZadaci listener){
        this.listener = listener;
    }
}


package com.example.zadatak15_atos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import kotlin.jvm.internal.Intrinsics;

public class AddEditZaposlenikActivity extends AppCompatActivity {
    public static final String EXTRA_ID_ZAPOSLENIK =
            "com.example.zadatak15_atos.EXTRA_ID_ZAPOSLENIK";
    public static final String EXTRA_IME =
            "com.example.zadatak15_atos.EXTRA_IME";
    public static final String EXTRA_PREZIME =
            "com.example.zadatak15_atos.EXTRA_PREZIME";
    public static final String EXTRA_RADNOMJESTO =
            "com.example.zadatak15_atos.EXTRA_RADNOMJESTO";
    public static final String EXTRA_OIB =
            "com.example.zadatak15_atos.EXTRA_OIB";

    private EditText editIme;
    private EditText editPrezime;
    private EditText editRadnoMjesto;
    private EditText editOib;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_zaposlenik);

        editIme = findViewById(R.id.edit_ime);
        editPrezime = findViewById(R.id.edit_prezime);
        editRadnoMjesto = findViewById(R.id.edit_radnomjesto);
        editOib = findViewById(R.id.edit_oib);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        Intent intent = getIntent();

        if(intent.hasExtra(EXTRA_ID_ZAPOSLENIK)){
            setTitle("Uredite zaposlenika");
            editIme.setText(intent.getStringExtra(EXTRA_IME));
            editPrezime.setText(intent.getStringExtra(EXTRA_PREZIME));
            editRadnoMjesto.setText(intent.getStringExtra(EXTRA_RADNOMJESTO));
            editOib.setText(intent.getStringExtra(EXTRA_OIB));
        }else {
            setTitle("Dodajte zaposlenika");
        }

        ActionBar actionbar = this.getSupportActionBar();
        Intrinsics.checkNotNull(actionbar);
        actionbar.setTitle((CharSequence)"Zadaci");
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    public boolean onSupportNavigateUp() {
        this.onBackPressed();
        String uloga = this.getIntent().getStringExtra("Uloga");
        return Boolean.parseBoolean(uloga);
    }
    
    private void saveZaposlenik(){
        String ime = editIme.getText().toString();
        String prezime = editPrezime.getText().toString();
        String radnoMjesto = editRadnoMjesto.getText().toString();
        String oib = editOib.getText().toString();

        if(ime.trim().isEmpty() || prezime.trim().isEmpty() || radnoMjesto.trim().isEmpty() || oib.trim().isEmpty()){
            Toast.makeText(this, "Unesite tražene podatke", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_IME, ime);
        data.putExtra(EXTRA_PREZIME, prezime);
        data.putExtra(EXTRA_RADNOMJESTO, radnoMjesto);
        data.putExtra(EXTRA_OIB, oib);

        int id = getIntent().getIntExtra(EXTRA_ID_ZAPOSLENIK, -1);
        if(id != -1){
            data.putExtra(EXTRA_ID_ZAPOSLENIK, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_zaposlenik_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.save_zaposlenik:
                saveZaposlenik();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}